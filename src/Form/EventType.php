<?php

namespace App\Form;

use App\Entity\Event;
use App\Form\CategoryEventType;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('description', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('dateBegin', DateTimeType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('dateEnd', DateTimeType::class, [
                'attr' => ['class' => 'form-control']
            ])
            ->add('place', TextType::class, [
                'attr' => ['class' => 'form-control']
            ])
            // ->add('idEventAccount')
            // ->add('idCatEvent', CategoryEventType::class);
            ->add('idCatEvent', CategoryEventType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
