<?php

namespace App\Form;

use App\Entity\Account;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class InscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('mail', EmailType::class, ['attr' => ['class' => "contain_input", 'placeholder' => "Email"]])
            ->add('username', TextType::class, ['attr' => ['class' => "contain_input", 'placeholder' => "Nom Utilisateur"]])
            ->add('password', PasswordType::class, ['attr' => ['class' => "contain_input", 'placeholder' => "Mot de Passe"]])
            ->add('confirm_password', PasswordType::class, ['attr' => ['class' => "contain_input", 'placeholder' => "confirmez le mot de passe"]]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Account::class,
        ]);
    }
}
