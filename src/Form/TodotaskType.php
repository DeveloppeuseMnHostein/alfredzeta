<?php

namespace App\Form;

use App\Entity\Todolist;
use App\Entity\Todotask;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TodotaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {




        $builder
            ->add('taskname')
            // ->add('idList', TodolistType::class);
            // ->add('toto')
            ->add('idList', EntityType::class, [
                // 'class' => Todolist::class,
                'class' => Todolist::class,
                'choice_label' => 'name',
                'mapped' => false,
                // 'choices' => 
                // 'multiple' => true
                // 'expanded' => false,
                // "property_path" => false
            ]);
        // ->add('idList', EntityType::class, [
        //     'class' => Todolist::class,
        //     'choice_label' => 'name',
        //     // 'choices' => 
        //     // 'multiple' => true
        // ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Todotask::class,
        ]);
    }
}
