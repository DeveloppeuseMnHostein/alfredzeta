<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Informations
 *
 * @ORM\Table(name="informations", indexes={@ORM\Index(name="fk_cat_task_idx", columns={"id_info_cat"}), @ORM\Index(name="fk_info_account_idx", columns={"id_info_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\InformationsRepository")
 */
class Informations
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=220, nullable=false)
     */
    private string $url;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text", nullable=false)
     */
    private string $text;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=false)
     */
    private $picture;

    /**
     * @var \TaskCategory
     *
     * @ORM\ManyToOne(targetEntity="TaskCategory", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_cat", referencedColumnName="id")
     * })
     */
    private $idInfoCat;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_info_account", referencedColumnName="id")
     * })
     */
    private $idInfoAccount;

    public function __construct()
    {
        $this->idInfoAccount = new Account();
        $this->idInfoCat = new TaskCategory();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getIdInfoCat(): ?TaskCategory
    {
        return $this->idInfoCat;
    }

    public function setIdInfoCat(?TaskCategory $idInfoCat): self
    {
        $this->idInfoCat = $idInfoCat;

        return $this;
    }

    public function getIdInfoAccount(): ?Account
    {
        return $this->idInfoAccount;
    }

    public function setIdInfoAccount(?Account $idInfoAccount): self
    {
        $this->idInfoAccount = $idInfoAccount;

        return $this;
    }
}
