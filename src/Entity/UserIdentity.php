<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Account;
use App\Entity\Language;
use App\Entity\Timezone;

/**
 * UserIdentity
 *
 * @ORM\Table(name="user_identity", indexes={@ORM\Index(name="fk_language_idx", columns={"id_language"}), @ORM\Index(name="fk_id_user_idx", columns={"id_user"}), @ORM\Index(name="fk_timezone_idx", columns={"id_fuseau"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserIdentityRepository")
 */
class UserIdentity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     *  @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=45, nullable=false)
     */
    private ?string $surname = "";

    /**
     * @var string
     *
     * @ORM\Column(name="portrait", type="string", length=255, nullable=false)
     */
    private $portrait;

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_user", referencedColumnName="id")
     * })
     */
    private $idUser;

    /**
     * @var \Timezone
     *
     * @ORM\ManyToOne(targetEntity="Timezone", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_fuseau", referencedColumnName="id")
     * })
     */
    private $idFuseau;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_language", referencedColumnName="id")
     * })
     */
    private $idLanguage;

    public function __construct()
    {
        $this->idLanguage = new Language();
        $this->idFuseau = new Timezone();
        // $this->idUser = new UserIdentity();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPortrait(): ?string
    {
        return $this->portrait;
    }

    public function setPortrait(string $portrait): self
    {
        $this->portrait = $portrait;

        return $this;
    }

    public function getIdUser(): ?Account
    {
        return $this->idUser;
    }

    public function setIdUser(?Account $idUser): self
    {
        $this->idUser = $idUser;

        return $this;
    }

    public function getIdFuseau(): ?Timezone
    {
        return $this->idFuseau;
    }

    public function setIdFuseau(?Timezone $idFuseau): self
    {
        $this->idFuseau = $idFuseau;

        return $this;
    }

    public function getIdLanguage(): ?Language
    {
        return $this->idLanguage;
    }

    public function setIdLanguage(?Language $idLanguage): self
    {
        $this->idLanguage = $idLanguage;

        return $this;
    }
}
