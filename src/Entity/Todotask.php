<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Todolist;

/**
 * Todotask
 *
 * @ORM\Table(name="todotask", indexes={@ORM\Index(name="fk_todolist_idx", columns={"id_list"})})
 * @ORM\Entity(repositoryClass="App\Repository\TodotaskRepository")
 */
class Todotask
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="taskname", type="string", length=45, nullable=false)
     */
    private ?string $taskname = "";

    /**
     * 
     * 
     * @var \Todolist
     *
    //  * @ORM\ManyToOne(targetEntity="Todolist", inversedBy="Todolist", cascade={"persist"})
     * @ORM\ManyToOne(targetEntity="Todolist", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list", referencedColumnName="id")
     * })
     */
    private $idList;
    // /**
    //  * 
    //  * 
    //  * @var \Todolist
    //  *
    //  * @ORM\OneToMany(targetEntity="Todolist", mappedBy="Todolist",cascade={"persist"})
    //  */
    // private $idList;

    public function __construct()
    {
        $this->idList = new Todolist();
    }

    public function __toString()
    {
        return strval($this->taskname);
        // return strval($this->idList);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTaskname(): ?string
    {
        return $this->taskname;
    }

    public function setTaskname(string $taskname): self
    {
        $this->taskname = $taskname;

        return $this;
    }

    public function getIdList(): ?Todolist
    {
        return $this->idList;
    }

    public function setIdList(?Todolist $idList): self
    {
        $this->idList = $idList;

        return $this;
    }
}
