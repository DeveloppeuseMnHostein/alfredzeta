<?php

namespace App\Entity;

use App\Entity\Todotask;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Todolist
 *
 * @ORM\Table(name="todolist", indexes={@ORM\Index(name="fk_cattask_idx", columns={"id_cat_list"}), @ORM\Index(name="fk_account_idx", columns={"id_list_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\TodolistRepository")
 */
class Todolist
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private ?string $name = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_list_account", referencedColumnName="id")
     * })
     */
    private $idListAccount;

    /**
     * @var \TaskCategory
     *
     * @ORM\ManyToOne(targetEntity="TaskCategory", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat_list", referencedColumnName="id")
     * })
     */
    private $idCatList;


    // /**
    //  * @var \Todotask
    //  *
    //  * @ORM\OneToMany(targetEntity="Todotask", mappedBy="Todotask", cascade={"persist"})
    //  */
    // private $todotasks;



    public function __construct()
    {
        $this->idListAccount = new Account();
        $this->idCatList = new TaskCategory();
    }

    public function __toString()
    {
        return $this->name;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    // public function getTodotasks(): ?Todotask
    // {
    //     return $this->todotasks;
    // }

    // public function setTodotasks(string $todotasks): self
    // {
    //     $this->todotasks[] = $todotasks;

    //     return $this;
    // }
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIdListAccount(): ?Account
    {
        return $this->idListAccount;
    }

    public function setIdListAccount(?Account $idListAccount): self
    {
        $this->idListAccount = $idListAccount;

        return $this;
    }

    public function getIdCatList(): ?TaskCategory
    {
        return $this->idCatList;
    }

    public function setIdCatList(?TaskCategory $idCatList): self
    {
        $this->idCatList = $idCatList;

        return $this;
    }
}
