<?php

namespace App\Entity;

use DateTimeInterface;
use App\Entity\Account;
use App\Entity\CategoryEvent;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Event
 *
 * @ORM\Table(name="event", indexes={@ORM\Index(name="fk_event_cat_idx", columns={"id_cat_event"}), @ORM\Index(name="fk_event_account_idx", columns={"id_event_account"})})
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "The name must be at least {{ limit }} characters long",
     *      maxMessage = "The name cannot be longer than {{ limit }} characters"
     * )
     * 
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private ?string $description = "";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_begin", type="datetime", nullable=false)
     */
    private ?\DateTime $dateBegin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=false)
     */
    private ?\DateTime $dateEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="place", type="string", length=120, nullable=false)
     */
    private ?string $place = "";

    /**
     * @var \Account
     *
     * @ORM\ManyToOne(targetEntity="Account",cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_event_account", referencedColumnName="id")
     * })
     */
    private $idEventAccount;

    /**
     * @var \CategoryEvent
     *
     * @ORM\ManyToOne(targetEntity="CategoryEvent", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cat_event", referencedColumnName="id")
     * })
     */
    private $idCatEvent;

    public function __construct()
    {
        $this->idEventAccount = new Account();
        $this->idCatEvent = new CategoryEvent(); //je crée des objets vides
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateBegin(): ?\DateTimeInterface
    {
        return $this->dateBegin;
    }

    public function setDateBegin(\DateTimeInterface $dateBegin): self
    {
        $this->dateBegin = $dateBegin;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getIdEventAccount(): ?Account
    {
        return $this->idEventAccount;
    }

    public function setIdEventAccount(?Account $idEventAccount): self
    {
        $this->idEventAccount = $idEventAccount;

        return $this;
    }

    public function getIdCatEvent(): ?CategoryEvent
    {
        return $this->idCatEvent;
    }

    public function setIdCatEvent(?CategoryEvent $idCatEvent): self
    {
        $this->idCatEvent = $idCatEvent;

        return $this;
    }

    public function __toString()
    {
        return strval($this->id);
    }
}
