<?php

namespace App\Controller;

use App\Entity\Todolist;
use App\Form\TodolistType;
use App\Repository\TodolistRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/todolist")
 */
class TodolistController extends AbstractController
{
    /**
     * @Route("/", name="app_todolist_index", methods={"GET"})
     */
    public function index(TodolistRepository $todolistRepository, EntityManagerInterface $entityManager): Response
    {

        $todolists = $entityManager
            ->getRepository(Todolist::class)
            ->findBy([
                "idListAccount" => $this->getUser() /// Récupère l'utilisateur connecter
            ]);
        return $this->render('todolist/index.html.twig', [
            "todolists" => $todolists
        ]);

        return $this->render('todolist/index.html.twig', [
            'todolists' => $todolistRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_todolist_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TodolistRepository $todolistRepository): Response
    {
        $todolist = new Todolist();
        $todolist->setIdListAccount($this->getUser());
        $form = $this->createForm(TodolistType::class, $todolist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todolistRepository->add($todolist);
            return $this->redirectToRoute('app_todolist_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('todolist/new.html.twig', [
            'todolist' => $todolist,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_todolist_show", methods={"GET"})
     */
    public function show(Todolist $todolist): Response
    {
        return $this->render('todolist/show.html.twig', [
            'todolist' => $todolist,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_todolist_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Todolist $todolist, TodolistRepository $todolistRepository): Response
    {
        $form = $this->createForm(TodolistType::class, $todolist);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todolistRepository->add($todolist);
            return $this->redirectToRoute('app_todolist_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('todolist/edit.html.twig', [
            'todolist' => $todolist,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_todolist_delete", methods={"POST"})
     */
    public function delete(Request $request, Todolist $todolist, TodolistRepository $todolistRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $todolist->getId(), $request->request->get('_token'))) {
            $todolistRepository->remove($todolist);
        }

        return $this->redirectToRoute('app_todolist_index', [], Response::HTTP_SEE_OTHER);
    }
}
