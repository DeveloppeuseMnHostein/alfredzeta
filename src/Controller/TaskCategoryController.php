<?php

namespace App\Controller;

use App\Entity\TaskCategory;
use App\Form\TaskCategoryType;
use App\Repository\TaskCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/task/category")
 */
class TaskCategoryController extends AbstractController
{
    /**
     * @Route("/", name="app_task_category_index", methods={"GET"})
     */
    public function index(TaskCategoryRepository $taskCategoryRepository): Response
    {
        return $this->render('task_category/index.html.twig', [
            'task_categories' => $taskCategoryRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_task_category_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TaskCategoryRepository $taskCategoryRepository): Response
    {
        $taskCategory = new TaskCategory();
        $form = $this->createForm(TaskCategoryType::class, $taskCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskCategoryRepository->add($taskCategory);
            return $this->redirectToRoute('app_task_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task_category/new.html.twig', [
            'task_category' => $taskCategory,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_task_category_show", methods={"GET"})
     */
    public function show(TaskCategory $taskCategory): Response
    {
        return $this->render('task_category/show.html.twig', [
            'task_category' => $taskCategory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_task_category_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TaskCategory $taskCategory, TaskCategoryRepository $taskCategoryRepository): Response
    {
        $form = $this->createForm(TaskCategoryType::class, $taskCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $taskCategoryRepository->add($taskCategory);
            return $this->redirectToRoute('app_task_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('task_category/edit.html.twig', [
            'task_category' => $taskCategory,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_task_category_delete", methods={"POST"})
     */
    public function delete(Request $request, TaskCategory $taskCategory, TaskCategoryRepository $taskCategoryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$taskCategory->getId(), $request->request->get('_token'))) {
            $taskCategoryRepository->remove($taskCategory);
        }

        return $this->redirectToRoute('app_task_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
