<?php

namespace App\Controller;

use App\Entity\Todolist;
use App\Entity\Todotask;
use App\Form\TodotaskType;
use App\Repository\TodotaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



/**
 * @Route("/todotask")
 */
class TodotaskController extends AbstractController
{
    /**
     * @Route("/", name="app_todotask_index", methods={"GET"})
     */
    public function index(TodotaskRepository $todotaskRepository): Response
    {
        // $toto = $todotaskRepository->findAll();
        $toto = $todotaskRepository->createQueryBuilder('todo')->leftJoin('todo.idList', 'fktodo')->getQuery()->getResult();
        dump($toto);
        return $this->render('todotask/index.html.twig', [
            'todotasks' => $toto,
        ]);
    }

    /**
     * @Route("/new", name="app_todotask_new", methods={"GET", "POST"})
     */
    public function new(Request $request, TodotaskRepository $todotaskRepository): Response
    {
        $todotask = new Todotask();
        $form = $this->createForm(TodotaskType::class, $todotask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todotask->setIdList($form->get('idList')->getData());
            $todotaskRepository->add($todotask);
            return $this->redirectToRoute('app_todotask_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('todotask/new.html.twig', [
            'todotask' => $todotask,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_todotask_show", methods={"GET"})
     */
    public function show(Todotask $todotask): Response
    {
        return $this->render('todotask/show.html.twig', [
            'todotask' => $todotask,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_todotask_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Todotask $todotask, TodotaskRepository $todotaskRepository): Response
    {
        $form = $this->createForm(TodotaskType::class, $todotask);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todotaskRepository->add($todotask);
            return $this->redirectToRoute('app_todotask_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('todotask/edit.html.twig', [
            'todotask' => $todotask,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_todotask_delete", methods={"POST"})
     */
    public function delete(Request $request, Todotask $todotask, TodotaskRepository $todotaskRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $todotask->getId(), $request->request->get('_token'))) {
            $todotaskRepository->remove($todotask);
        }

        return $this->redirectToRoute('app_todotask_index', [], Response::HTTP_SEE_OTHER);
    }
}
