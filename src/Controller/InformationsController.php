<?php

namespace App\Controller;

use App\Entity\Informations;
use App\Form\InformationsType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\InformationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/informations")
 */
class InformationsController extends AbstractController
{
    /**
     * @Route("/", name="app_informations_index", methods={"GET"})
     */
    public function index(InformationsRepository $informationsRepository, EntityManagerInterface $entityManager): Response
    {
        $informations = $entityManager
            ->getRepository(Informations::class)
            ->findBy([
                "idInfoAccount" => $this->getUser() /// Récupère l'utilisateur connecter
            ]);
        return $this->render('informations/index.html.twig', [
            "informations" => $informations
        ]);

        return $this->render('informations/index.html.twig', [
            'informations' => $informationsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_informations_new", methods={"GET", "POST"})
     */
    public function new(Request $request, InformationsRepository $informationsRepository): Response
    {
        $information = new Informations();
        $information->setIdInfoAccount($this->getUser());
        $form = $this->createForm(InformationsType::class, $information);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $informationsRepository->add($information);
            return $this->redirectToRoute('app_informations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('informations/new.html.twig', [
            'information' => $information,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_informations_show", methods={"GET"})
     */
    public function show(Informations $information): Response
    {
        return $this->render('informations/show.html.twig', [
            'information' => $information,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_informations_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Informations $information, InformationsRepository $informationsRepository): Response
    {
        $form = $this->createForm(InformationsType::class, $information);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $informationsRepository->add($information);
            return $this->redirectToRoute('app_informations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('informations/edit.html.twig', [
            'information' => $information,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_informations_delete", methods={"POST"})
     */
    public function delete(Request $request, Informations $information, InformationsRepository $informationsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $information->getId(), $request->request->get('_token'))) {
            $informationsRepository->remove($information);
        }

        return $this->redirectToRoute('app_informations_index', [], Response::HTTP_SEE_OTHER);
    }
}
