<?php

namespace App\Controller;


use App\Entity\Account;
use App\Form\InscriptionType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends AbstractController
{

    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $account = new Account();
        $form = $this->createForm(InscriptionType::class, $account);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On encode le mot de passe et on envoie les données saisis de l'utilisateur.
            $hash = $encoder->encodePassword($account, $account->getPassword());
            $account->setPassword($hash);
            $manager->persist($account);
            $manager->flush($account);

            $this->addFlash('create', "votre compte à bien été crée !");
            return $this->redirectToRoute('app_home');
        }

        return $this->render('security/inscription.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion(): Response
    {
        return $this->render('security/connexion.html.twig', []);
    }

    /** 
     * @Route("/deconnexion", name="deconnexion") 
     */
    public function deconnexion()
    {
        return $this->redirectToRoute('app_home');
    }
}
