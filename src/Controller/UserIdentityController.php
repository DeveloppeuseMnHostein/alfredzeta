<?php

namespace App\Controller;

use App\Entity\UserIdentity;
use App\Form\UserIdentityType;
use App\Repository\UserIdentityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/identity")
 */
class UserIdentityController extends AbstractController
{
    /**
     * @Route("/", name="app_user_identity_index", methods={"GET"})
     */
    public function index(UserIdentityRepository $userIdentityRepository): Response
    {
        return $this->render('user_identity/index.html.twig', [
            'user_identities' => $userIdentityRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_user_identity_new", methods={"GET", "POST"})
     */
    public function new(Request $request, UserIdentityRepository $userIdentityRepository): Response
    {
        $userIdentity = new UserIdentity();
        $form = $this->createForm(UserIdentityType::class, $userIdentity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userIdentityRepository->add($userIdentity);
            return $this->redirectToRoute('app_user_identity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user_identity/new.html.twig', [
            'user_identity' => $userIdentity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_identity_show", methods={"GET"})
     */
    public function show(UserIdentity $userIdentity): Response
    {
        return $this->render('user_identity/show.html.twig', [
            'user_identity' => $userIdentity,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_user_identity_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, UserIdentity $userIdentity, UserIdentityRepository $userIdentityRepository): Response
    {
        $form = $this->createForm(UserIdentityType::class, $userIdentity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userIdentityRepository->add($userIdentity);
            return $this->redirectToRoute('app_user_identity_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user_identity/edit.html.twig', [
            'user_identity' => $userIdentity,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_user_identity_delete", methods={"POST"})
     */
    public function delete(Request $request, UserIdentity $userIdentity, UserIdentityRepository $userIdentityRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userIdentity->getId(), $request->request->get('_token'))) {
            $userIdentityRepository->remove($userIdentity);
        }

        return $this->redirectToRoute('app_user_identity_index', [], Response::HTTP_SEE_OTHER);
    }
}
